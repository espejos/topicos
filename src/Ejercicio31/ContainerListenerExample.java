/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio31;
import java.awt.Component;
import java.awt.Container;
import java.awt.GridLayout;  // import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener; //import java.awt.event.*;
import javax.swing.JButton;
import javax.swing.JFrame;    // import javax.swing.*;
/**
 *
 * @author LORENAC2
 */
public class ContainerListenerExample {
  JFrame jFrame = new JFrame();
  Container cPane = jFrame.getContentPane();
  ContainerListener containerListener = new OyenteContenedor();
  JButton [] boton = new JButton[5]; 
  String [] letras = {  "First", "Second", "Third", "Fourth", "Fifth"  };

  class OyenteContenedor implements ContainerListener{
    ActionListener actiListener = new ActionListener() {  // clase anonima
      @Override
      public void actionPerformed(ActionEvent event) {
        System.out.println("Select: " + event.getActionCommand());
        for (int i=0; i < 5; i++) 
          if (event.getActionCommand()==letras[i])
            cPane.remove(boton[i]);
      }
    };

    @Override
    public void componentAdded(ContainerEvent event) {
      Component compChild = event.getChild();

      if (compChild instanceof JButton) {
        JButton jButton = (JButton) compChild;
 
        jButton.addActionListener(actiListener);
        System.out.println("Oyente de accion agregado al boton");       
      }
    }

    @Override
    public void componentRemoved(ContainerEvent event) {
      Component compChild = event.getChild();

      if (compChild instanceof JButton) {
        JButton Jbutton = (JButton) compChild;

        Jbutton.removeActionListener(actiListener);
        cPane.remove(compChild);
        System.out.println("Se retiro del boton el oyente de accion");
      }
    }
  }  
    
  public static void main(String args[]) {             
    ContainerListenerExample cle = new ContainerListenerExample();

    cle.cPane.addContainerListener(cle.containerListener);
    cle.cPane.setLayout(new GridLayout(3, 2));
    for (int i =0; i < 5; i++) {
      cle.boton[i] = new JButton(cle.letras[i]);
      cle.cPane.add(cle.boton[i]);
    }
    cle.jFrame.setSize(400, 300);
    cle.jFrame.show();
  }
}


