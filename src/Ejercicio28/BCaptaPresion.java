/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio28;
import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author LORENAC2
 */
class BCaptaPresion implements ActionListener {
  private JTextArea textArea;

  public BCaptaPresion(JTextArea ta) {  textArea = ta;  }

  @Override
  public void actionPerformed(ActionEvent event) {
    textArea.append("Button was pressed\n");
  }
}
