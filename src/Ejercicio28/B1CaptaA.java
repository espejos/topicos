/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio28;
 import javax.swing.*;
  import java.awt.event.*;
  import java.util.*;
/**
 *
 * @author LORENAC2
 */
  class B1CaptaA implements ActionListener {
    private JButton button2;
    private JTextArea textArea;
    private java.util.List list;
    private int i;

    public B1CaptaA(List l, JButton b, JTextArea t, int i) {
      button2 = b;
      textArea = t;
      list = l;
      this.i = i;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
      ActionListener actListener = new EventCounter(i++, textArea);

      textArea.append("A pressed\n");
      list.add(actListener);
      button2.addActionListener(actListener);
    }
  }