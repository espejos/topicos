/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio28;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
/**
 *
 * @author LORENAC2
 */
  class B2CaptaB implements ActionListener {
    private java.util.List list;
    private JButton button2;
    JTextArea textArea;

    public B2CaptaB (JButton b, List l, JTextArea jta) {
      list = l;
      button2 = b;
      textArea = jta;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
      int end = list.size() - 1;

      textArea.append("B pressed\n");
      if (end >= 0) {
        button2.removeActionListener((ActionListener) list.get(end));
        list.remove(end);
      }
    }
 }