/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio28;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JOptionPane;
/**
 *
 * @author LORENAC2
 */
class ActionListenerExample {
  public static void main(String args[]) {
    Frame frame = new Frame("ActionListenerExample ");
    ActionListener buttonListener = new MyActionListener();
    Button button1 = new Button("A");
    Panel p = new Panel();
    
    p.add(button1);
    frame.add(button1, BorderLayout.NORTH);
    button1.addActionListener(buttonListener);
    frame.pack();
    frame.setVisible(true);
    
    frame.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });
  }
}

class MyActionListener implements ActionListener {
  public void actionPerformed(ActionEvent event) {
    String choice = event.getActionCommand();
    
    JOptionPane.showMessageDialog(null, "You have clicked: " + choice);
  }
}

