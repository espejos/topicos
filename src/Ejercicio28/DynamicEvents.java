/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio28;
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
/**
 *
 * @author LORENAC2
 */
public class DynamicEvents extends JApplet {
  private java.util.List list = new ArrayList();
  private int i = 0;
  private JButton button1 = new JButton("A");
  private JButton button2 = new JButton("B");
  private JTextArea textArea = new JTextArea();

  @Override
  public void init() {
    Container container = getContentPane();
    JPanel jPanel = new JPanel();

    button1.addActionListener(new BCaptaPresion(textArea));
    button1.addActionListener(new B1CaptaA(list, button2, textArea, i));
    button2.addActionListener(new BCaptaPresion(textArea));
    button2.addActionListener(new B2CaptaB(button2, list, textArea));
    jPanel.add(button1);
    jPanel.add(button2);
    container.add(BorderLayout.NORTH, jPanel);
    container.add(new JScrollPane(textArea));
  }

  public static void main(String[] args) {
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        showUI(new DynamicEvents(),800,600);  
      }
    });
  }

  public static void showUI(JApplet app, int w, int h) {
    JFrame jFrame = new JFrame();

    jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    jFrame.getContentPane().add(app);
    jFrame.setSize(w, h);
    app.init();
    app.start();
    jFrame.setVisible(true);
  }
}

