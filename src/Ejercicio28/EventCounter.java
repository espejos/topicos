/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio28;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 *
 * @author LORENAC2
 */
class EventCounter implements ActionListener {
  private int index;
  private JTextArea textArea;

  public EventCounter(int indx, JTextArea jta) {  
    index = indx;  
    textArea = jta;
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    textArea.append("Counted Listener " + index + "\n");
  }
}