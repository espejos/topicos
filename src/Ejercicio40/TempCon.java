/*TempCon.java
Protocolo acostumbrado
por cirino silva tovar
el 22 de septiembre del 2015
para ilustrar el uso de ActionEvent y addActionListener y actionPerformed()
tambien ActionListener
Ejercicio 40
 */

package Ejercicio40;
import Ejercicio19.VentanaCerrable;
import java.awt.*;    // usar componente GUI de la 1a generacion 
import javax.swing.*; // usar componentes ligeros de la 2a generacion
import java.awt.event.*;  // manejar eventos 
/**
 *
 * @author LORENAC2
 */
public class TempCon implements ActionListener {   // es un oyente de accion 3er postulado y 7o postulado
  private Label centLabel, farLabel;               
  private TextField centigrados, farenheit;
  private Button b;    // fuente de eventos de accion 2o postulado
  private double c, f;
  private Frame frame;          // fuente de evento de ventana  2o.posulado

  public void inicio() {                             // cortinilla
        JFrame frame = new JFrame();                     // Contenedor 
        ImageIcon peli = new ImageIcon(getClass().getResource("Tierra10b.jpg"));
        JLabel imagenS = new JLabel(peli);               // etiqueta con la imagen

        System.out.println("Convierte of a oc y viceversa");    
        frame.add(imagenS);          // Add a component with a custom paint method
        frame.setLocation(300, 300); 
        frame.setSize(500, 500);
        frame.setUndecorated(true);  // no despliega la cintilla de titulo del frame
        frame.setVisible(true);      // Display the frame

        try { Thread.sleep(4000); }  // duerme el hilo principal 2 segundos
        catch (InterruptedException e) {}

        frame.setVisible(false);
  }

  public void datos() {
    centLabel = new Label("Temperatura en oC:");
    centigrados = new TextField(5);
    farLabel = new Label("Temperatura en oF:");
    farenheit = new TextField(5);
    b = new Button("Convertir"); // boton fuente de eventos de accion 2o. postulado  
    b.addActionListener(this);   // registra el oyente en el boton 6o postulado para accion
  }

  public void launchFrame() {
    frame = new VentanaCerrable("Temperatura");   // WindowEvent  1er postulado  3er postulado y 6o 
    frame.setLayout(new FlowLayout());            // postulado para ventana
    frame.add(centLabel);
    frame.add(centigrados);
    frame.add(farLabel);
    frame.add(farenheit);
    frame.add(b);
    frame.setSize(300,200);
    frame.pack();
    frame.setVisible(true);   // 5o postulado se genera un Event-Thread
  }

  public boolean isNum(String cad) {
    if (cad != null && cad.length() > 0)
      try {
        Double.parseDouble(cad);
        return true;
      } catch (NumberFormatException nfe) {
          JOptionPane.showMessageDialog(null, "No se haga el gracioso");        
        }
    return false;
  }

  public void actionPerformed(ActionEvent e) {   //event handler Evento de Accion primer postulado
    String aux, far;                        // cuarto postulado

    aux = centigrados.getText();
    far = farenheit.getText();
    if (isNum(aux))  {       
        c = Double.parseDouble(aux);
        f = 9*c/5 + 32;
        farenheit.setText(Double.toString(f));  
    } else if (isNum(far)) {
             f = Double.parseDouble(far);
             c = (f-32)*5/9;
             centigrados.setText(Double.toString(c));
           }
  }

  public static void main(String[] args) {
    TempCon tfc = new TempCon();

    tfc.inicio();
    tfc.datos();
    // ojo los calculos los invoca actionPerformed();
    tfc.launchFrame();   // los resultados desplegados en el frame
    // entre bambalinas la jvm maneja el hilo de despacho de eventos
    // e implementa la navegabilidad
  }
} 



