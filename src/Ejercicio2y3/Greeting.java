/*
Claudia Lorena Cervera Canchola
Ejercicio 2 y 3
Meta: Presentar la visualizacion de los componentes del paquete java.awt.* y javax.swing.*
Datos: Contenedores JFrame y JTextFile (Aqui mismo se resuelve el ejercicio 3)
Calculos: No hay puesto que no requiere eventos.
 Salidas: Una ventana grafica con un mensaje de texto
 Navegabilidad: Solo acciones de iconizacion
 */
package Ejercicio2y3;

import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import static javax.swing.JFrame.EXIT_ON_CLOSE;
import javax.swing.JTextField;

/**
 *
 * @author LORENAC2
 */
public class Greeting extends JFrame {
    private JTextField textField;
    public static void main (String[] args) { 
            Greeting frame = new Greeting(); 
            frame.setSize(300, 300); 
            frame.createGUI(); 
            frame.setVisible(true);
    }
    private void createGUI() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        Container window = getContentPane();
        window.setLayout(new FlowLayout() );
        textField = new JTextField("Hola \n"
                + "Claudia Lorena Cervera Canchola");
        window.add(textField);
    }
}