/*
Claudia Lorena Cervera Canchola
Ejercicio 44
 */
package Ejercicio44;
/**
 *
 * @author LORENAC2
 */
// Implementación de la interface Serializable del paquete java.io 
public class PersonaBean implements java.io.Serializable {

    // Cada variable de instancia tiene un modificador de acceso de tipo privado.
    private String nombre;
    private int edad;

    // Constructor vacío y sin argumentos.
    public PersonaBean() {
    }

    // Constructor opcional de un JavaBean.
    public PersonaBean(String nombre, int edad) {
        this.nombre = nombre;
        this.edad = edad;
    }

    // Constructor por copia (opcional)
    public PersonaBean(PersonaBean personaBean) {
        this.nombre = personaBean.getNombre();
        this.edad = personaBean.getEdad();
    }

    // Por cada propiedad agrega un método get y set.
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

}
