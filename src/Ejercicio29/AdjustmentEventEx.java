/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio29;
import java.awt.*;        // componentes
import java.awt.event.*;  // eventos e interfaces
import javax.swing.JOptionPane;
/**
 *
 * @author LORENAC2
 */
public class AdjustmentEventEx extends VentanaCerrable // 3er postulado oyente de eventos
                               implements AdjustmentListener {   // ajuste y ventana
  int rval=0,gval=0,bval=0;    // 7o postulado implenta interface
  Scrollbar sr,sb,sg;       // 3 Scroolbar
  Panel ps;                
  Label l1, l2, l3, reveal; // 4 etiquetas

  void inicio() {
    String msg = "Evento de ajuste y patrones de saturacion de color";
    
    JOptionPane.showMessageDialog(null,msg);
  }

  AdjustmentEventEx() {	    // constructor   fase 2 o entradas
    l1=new Label("Red");
    l2=new Label("Green");
    l3=new Label("Blue");	
    reveal = new Label();  
    sr=new Scrollbar(Scrollbar.HORIZONTAL,0,15,0,255);
    sg=new Scrollbar(Scrollbar.HORIZONTAL,0,15,0,255);
    sb=new Scrollbar(Scrollbar.HORIZONTAL,0,15,0,255);
    ps=new Panel();
  }

  void launchFrame() {   // fases 2 y 4 del alguiritmo
    ps.add(l1);
    ps.add(sr);
    ps.add(l2);
    ps.add(sg);
    ps.add(l3);
    ps.add(sb);
    sr.setPreferredSize(new Dimension(150, 40));
    sg.setPreferredSize(new Dimension(150, 40));
    sb.setPreferredSize(new Dimension(150, 40));
    add("South",ps);
    add(reveal);
    addWindowListener(this);             // cierre de la ventana
    sr.addAdjustmentListener(this);      // fuentes de evento de ajuste   
    sg.addAdjustmentListener(this);      // 2o postulado
    sb.addAdjustmentListener(this);      // oyentes registrados en las fuentes 6o. postulado
    setSize(600,600);
    setVisible(true);              // 5o postulado hilo de despacho de eventos
  }

  // este es el event Handler 4o. postulado  
  public void adjustmentValueChanged( AdjustmentEvent e)	{    // fase 3 eventos  
    if(e.getSource().equals(sr))		    // 1er postulado evento de ajuste
      rval=sr.getValue();
    if(e.getSource().equals(sg)) 
      gval=sg.getValue();
    if(e.getSource().equals(sb)) 
      bval=sb.getValue();
      setBackground(new Color(rval,gval,bval));
      reveal.setText("   rgb patern: "+ rval+","+gval+","+bval);
   }
  public static void main(String[] args) {          // naveacion usando ventanaCerrable
    AdjustmentEventEx ae=new AdjustmentEventEx();   // fuente de evento de ventana
    ae.inicio();                                                // 2o postulado
    ae.launchFrame();
  }
}















