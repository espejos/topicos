/*
Claudia Lorena Cervera Canchola
Ejercicio 8
Meta: Presentar la visualizacion de los componentes del paquete java.awt.*
Datos: Contenedores Frame y Panel
Calculos: No hay puesto que no requiere eventos.
 Salidas:Despliegue de los contenedores en la pantalla
 Navegabilidad: Solo acciones de iconizacion
 */
package Ejercicio8;

import java.awt.*;

/**
 *
 * @author LORENAC2
 */
public class FrameWithPanel {
    private Frame f; 
    private Panel pan; 
    
    public FrameWithPanel(String title) {
        f = new Frame(title); 
        pan = new Panel();
    }
    
    public void launchFrame() { 
        f.setSize(200,200);
        f.setBackground(Color.blue); 
        f.setLayout(null);  // Use default layout 
        f.setLocation(50, 50);
        pan.setSize(100,100); 
        pan.setBackground(Color.yellow); 
        f.add(pan); 
        f.setVisible(true);
    } 
        
    public static void main(String args[]) { 
        FrameWithPanel guiWindow = new FrameWithPanel("Frame with Panel"); 
        guiWindow.launchFrame(); //Navegabilidad
    }
}