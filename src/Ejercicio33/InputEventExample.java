/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio33;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Frame;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
/**
 *
 * @author LORENAC2
 */
class InputEventExample {                  
  public void launchFrame() {
    Frame frame = new Frame("InputEventExample");
    Button button = new Button("A");
    
    frame.add(button, BorderLayout.NORTH);
    frame.pack();
    frame.setVisible(true);
    button.addMouseListener(new MiEntrada());
    frame.addWindowListener(new WindowAdapter() {   // clase anonima
      public void windowClosing(WindowEvent e) {   System.exit(0);  }
    });
  }

  public class MiEntrada extends MouseAdapter { // clase interna
    public void mousePressed(MouseEvent me) {
      int mode = me.getModifiers();
        
      if ((mode & InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK) 
        System.out.println("Left button pressed.");
      if ((mode & InputEvent.BUTTON2_MASK) == InputEvent.BUTTON2_MASK) 
        System.out.println("Middle button pressed.");
      if ((mode & InputEvent.BUTTON3_MASK) == InputEvent.BUTTON3_MASK) 
        System.out.println("Right button pressed.");
    }

    public void mouseReleased(MouseEvent me) {
      System.out.println("mouseReleased " + me.getX());
    }
  }
  
  public static void main(String args[]) {
    InputEventExample iee = new InputEventExample();

    iee.launchFrame();
  }
}

