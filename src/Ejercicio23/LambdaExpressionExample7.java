/*
Claudia Lorena Cervera Canchola
Ejercicio 23
 */
package Ejercicio23;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LORENAC2
 */
public class LambdaExpressionExample7{  
    public static void main(String[] args) {  
          
        List<String> list=new ArrayList<String>();  
        list.add("ankit");  
        list.add("mayank");  
        list.add("irfan");  
        list.add("jai");  
          
        list.forEach(  
            (n)->System.out.println(n)  
        );  
    }  
}  
