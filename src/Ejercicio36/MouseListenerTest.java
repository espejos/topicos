/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio36;
import java.awt.*;
import java.applet.*;
import java.awt.event.*;
/**
 *
 * @author LORENAC2
 */
public class MouseListenerTest extends Applet implements MouseListener
{
        String msg="";
        int mx=0, my=0;
        public void init()
	{
                addMouseListener(this);
	}
        public void mouseClicked(MouseEvent me)
	{
                mx=me.getX();
                my=me.getY();
                msg="Mouse Clicked at Position "+mx+" : "+my;
                repaint();
	}
        public void mouseEntered(MouseEvent me)
        {
                mx=10;
                my=10;
                msg="Mouse Entered";
                repaint();
        }
        public void mouseExited(MouseEvent me)
        {
                mx=10;
                my=10;
                msg="Mouse Left";
                repaint();
        }
        public void mousePressed(MouseEvent me)
        {
        }
        public void mouseReleased(MouseEvent me)
        {
        }
        public void paint(Graphics g)
        {
                g.drawString(msg,mx,my);
        }
}

