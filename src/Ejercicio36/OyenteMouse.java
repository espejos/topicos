/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio36;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
/**
 *
 * @author LORENAC2
 */
public class OyenteMouse implements MouseListener{
  JPopupMenu popMenu;

  public OyenteMouse(JPopupMenu pop) {  popMenu = pop;  }
  @Override
  public void mouseClicked(MouseEvent event) {}
  @Override
  public void mouseEntered(MouseEvent event) {}
  @Override
  public void mouseExited(MouseEvent event) {}

  @Override
  public void mousePressed(MouseEvent event) {
    if (event.isPopupTrigger()) 
      popMenu(event);
  }

  @Override
  public void mouseReleased(MouseEvent event) {
    if (event.isPopupTrigger()) 
      popMenu(event);
  }

  private void popMenu(MouseEvent event) {
    popMenu.show(event.getComponent(), event.getX(), event.getY());
  }
}

