/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio36;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 *
 * @author LORENAC2
 */
public class OyenteMouse2 implements MouseListener {
  private MouseListenerExample f;

  public OyenteMouse2(MouseListenerExample f) {  this.f = f; }

  @Override
  public void mouseClicked(MouseEvent event) {}
  @Override
  public void mouseEntered(MouseEvent event) {}
  @Override
  public void mouseExited(MouseEvent event) {}
  
  @Override
  public void mousePressed(MouseEvent event) {    
    f.red_col = Color.RED;
    f.startX = f.endX = event.getX();
    f.startY = f.endY = event.getY();
    f.repaint();
  }

  @Override  
  public void mouseReleased(MouseEvent event) {
    f.red_col = Color.BLACK;
    f.repaint();
  }
}

