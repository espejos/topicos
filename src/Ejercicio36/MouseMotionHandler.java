/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio36;
import java.awt.*;
import java.awt.event.*;
/**
 *
 * @author LORENAC2
 */
class MouseMotionHandler extends MouseMotionAdapter {
  private Coordinates c;
  private Color myColor = Color.blue;

  public MouseMotionHandler(Coordinates c) {  this.c = c;  }

  //draws a line from last coordinates to current ones
  public void mouseDragged(MouseEvent e) {
    Graphics g = c.getGraphics(); 

    g.setColor(myColor);
    g.drawLine(c.getX(), c.getY(), e.getX(), e.getY());
    c.setX(e.getX());
    c.setY(e.getY());
    c.getStatus().setText("(Mouse is at:"+c.getX()+","+c.getY()+")");
  }
}

