/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio36;
import java.awt.*;
import java.awt.event.*;
/**
 *
 * @author LORENAC2
 */
class MouseClickHandler extends MouseAdapter {
  private Coordinates c;

  public MouseClickHandler(Coordinates c) {  this.c = c;  }
 
 //catches the mouse event to get first coordinates
  public void mousePressed(MouseEvent e) {
    c.setX(e.getX());
    c.setY(e.getY());
  }
}


