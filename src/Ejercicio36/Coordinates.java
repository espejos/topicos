/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio36;
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author LORENAC2
 */
public class Coordinates extends JFrame {
  private int lastX, lastY;
  //private Color myColor = Color.blue;
  private JLabel status;
  public void setX(int x) {  lastX = x;  }
  public void setY(int y) {  lastY = y;  }
  public int getX() {  return lastX;  }
  public int getY() {  return lastY;  }
  public void setStatus(JLabel s) {  status = s;  }
  public JLabel getStatus() {  return status;  }

  // setup background colour
  public Coordinates() {
    status = new JLabel();
    getContentPane().add(status, BorderLayout.SOUTH);
    addMouseListener(new MouseClickHandler(this));
    addMouseMotionListener(new MouseMotionHandler(this));
  }

  public static void main(String args[]) {
    JFrame f = new Coordinates();

    f.setSize(500, 500);
    f.show();
  }
}

