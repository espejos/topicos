/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio36;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 *
 * @author LORENAC2
 */
public class OyenteMotion implements MouseMotionListener{
  MouseListenerExample f;

  public OyenteMotion (MouseListenerExample f) {  this.f = f;  }

  @Override
  public void mouseDragged(MouseEvent event) {
    f.endX = event.getX();
    f.endY = event.getY();
    f.repaint();
  }

  @Override
  public void mouseMoved(MouseEvent event) {}
}

 
