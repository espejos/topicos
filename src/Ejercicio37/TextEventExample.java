/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio37;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.TextComponent;
import java.awt.TextField;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
/**
 *
 * @author LORENAC2
 */
class TextEventExample {
  public static void main(String args[]) {
    Frame frame = new Frame("TextEventExample");
    TextField textField = new TextField();
    frame.add(textField, BorderLayout.NORTH);
    frame.pack();
    frame.setVisible(true);
    textField.addTextListener(new MyTextListener("Text Field"));
    frame.addWindowListener(new WindowAdapter() {    // clase anonima
      public void windowClosing(WindowEvent we) {  System.exit(0);  }
    });
  }
}
class MyTextListener implements TextListener {  // oyente de texto
  String preface;

  public MyTextListener(String source) {
    preface = source + " text value changed.\n" + "   First 10 characters: \"";
  }
  public void textValueChanged(TextEvent e) {    // event handler
    TextComponent tc = (TextComponent) e.getSource();
    System.out.println("Typed value in TextComponent " + tc.getText());
  }
}







