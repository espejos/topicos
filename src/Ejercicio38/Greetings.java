/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio38;
import java.awt.*;
import javax.swing.*;
/**
 *
 * @author LORENAC2
 */
public class Greetings extends JFrame {
  private JTextField textField;

  public static void main (String[] args) {
    Greetings frame = new Greetings();

    frame.setSize(300, 200);
    frame.createGUI();
    frame.setVisible(true);
  }

  private void createGUI() {    
    //Container window = getContentPane();

    setDefaultCloseOperation(EXIT_ON_CLOSE);
    setLayout(new FlowLayout() );
    textField = new JTextField("Hello!");
    add(textField);
  }
}
