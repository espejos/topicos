/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio38;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import cstio.Pizarra;
 /**
 *
 * @author LORENAC2
 */
public class WinLsrnExp extends JFrame {
  Pizarra p = new Pizarra();
     
  public WinLsrnExp() {         
    setTitle("Window listener Example : "); 
    setSize(150, 150);
    p.setVisible(true);
  }

  public static void main(String[] args) {
    WinLsrnExp wle = new WinLsrnExp();
    

    wle.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    wle.setVisible(true);
    
    wle.addWindowListener(new OyenteWin(wle.p));
  }
}

class OyenteWin implements WindowListener {
  Pizarra p;

  public OyenteWin(Pizarra p) {  this.p = p; }
  public void windowClosed(WindowEvent w) { p.out("Window close occur\n");}
  public void windowActivated(WindowEvent w) {  p.out("Window Activated\n");  }
  public void windowClosing(WindowEvent we)  {  p.out("Window Closing\n");    }
  public void windowDeactivated(WindowEvent w) {p.out("Window Deactivated\n");}
  public void windowDeiconified(WindowEvent w) {p.out("Window Deiconified\n");}
  public void windowIconified(WindowEvent w) {  p.out("Window Iconified\n");  }
  public void windowOpened(WindowEvent w)    {  p.out("Window Opened\n");     }
}