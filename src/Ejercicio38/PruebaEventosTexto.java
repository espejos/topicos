/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio38;
import java.awt.*;
import java.awt.event.*;
/**
 *
 * @author LORENAC2
 */
class EventosTexto{
    TextField tf1;
    TextArea areaTexto;
    Frame v;
    String txt1,txt2;
    Label etiqueta1, etiqueta2;
    ManejaEventosTexto oyeTexto;
    ManejaEventosBoton oyeBoton;
    Button b;

    class ManejaEventosTexto implements TextListener{
       public void textValueChanged(TextEvent e){
          Object fuente=e.getSource();
          
          if (fuente==tf1){      txt1=new String(tf1.getText() );          }
          if (fuente==areaTexto){ txt2=new String(areaTexto.getText() );    }
       }
    }
    
    class ManejaEventosBoton implements ActionListener{
       public void actionPerformed(ActionEvent evento){
           Object fuente=evento.getSource();
           
           if(fuente==b) recuperaTexto();
       }
    }
    
    void forma() {
        v=new Frame();
        v.setLayout(new FlowLayout() );
        tf1 = new TextField(20);
        b=new Button("Mostrar Texto");
        areaTexto = new TextArea("",5, 40,TextArea.SCROLLBARS_NONE);
        areaTexto.setBackground(Color.white);
        oyeTexto=new ManejaEventosTexto();
        oyeBoton=new ManejaEventosBoton();
        tf1.addTextListener(oyeTexto);
        areaTexto.addTextListener(oyeTexto);
        b.addActionListener(oyeBoton);
        v.add(tf1);
        v.add(areaTexto);
        v.add(b);
        v.add(etiqueta1=new Label("Etiqueta 1"));        
        v.add(etiqueta2=new Label("Etiqueta 2"));
        v.pack();
        OyenteFrame oyente = new OyenteFrame();
        v.addWindowListener(oyente);
        v.setVisible(true);
   }
  
   void recuperaTexto() {           
       etiqueta1.setText(txt1);
       etiqueta2.setText(txt2);
   }
}

public class PruebaEventosTexto{
    public static void main(String args[]){
        EventosTexto pagina=new EventosTexto();
        
        pagina.forma();
    }
}

