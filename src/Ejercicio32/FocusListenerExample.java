/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio32;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
 /**
 *
 * @author LORENAC2
 */
public class FocusListenerExample {
  public static void main(String [] args) { 
    JFrame.setDefaultLookAndFeelDecorated(true);
    JFrame frame = new JFrame("FocusListener Example");

    frame.setSize(300,150); 
    final JLabel label = new JLabel();
    frame.add(label);
 
    frame.addFocusListener( new FocusListener() {  // clase anonima
        public void focusGained(FocusEvent fe){
          label.setText("Focus Gained"); 
        }
        public void focusLost(FocusEvent fe){
          label.setText("Focus Lost");
          System.out.println("Focus Lost");
        }
    });
 
    frame.setVisible(true);			
  }
}