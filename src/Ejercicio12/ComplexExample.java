/*
Claudia Lorena Cervera Canchola
Ejercicio 12
Meta: Presentar la visualizacion de los componentes del paquete java.awt.*
Datos: Contenedores Frame y Buttons
Calculos: No hay puesto que no requiere eventos.
Salidas:Despliegue de los contenedores en la pantalla
Navegabilidad: Solo acciones de iconizacion
 */
package Ejercicio12;
import java.awt.*;

/**
 *
 * @author LORENAC2
 */
public class ComplexExample {
    private Frame f;
    private Panel p;
    private Button bw, bc;
    private Button bfile, bhelp;
    
    public ComplexExample() {
        f = new Frame("GUI example 3");
        bw = new Button("West");
        bc = new Button("Work space region");
        bfile = new Button("File");
        bhelp = new Button("Help");
    }
    
    public void launchFrame() {
    // Add bw and bc buttons in the frame border 
    f.add(bw, BorderLayout.WEST); 
    f.add(bc, BorderLayout.CENTER); 
    // Create panel for the buttons in the north border
    p = new Panel();
    p.add(bfile);
    p.add(bhelp);
    f.add(p, BorderLayout.NORTH);
    // Pack the frame and make it visible
    f.pack();
    f.setVisible(true);
    }
    public static void main(String args[]) {
        ComplexExample gui = new ComplexExample();
        gui.launchFrame();
    }
}