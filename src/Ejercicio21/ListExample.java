/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio21;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author LORENAC2
 */
public class ListExample {
     public static void main(String[] args) { 
        List list = new ArrayList();
        list.add("one"); 
        list.add("second");
        list.add("3rd");
        list.add(new Integer(4)); 
        list.add(new Float(5.0F));
        list.add("second");// duplicate, is added 
        list.add(new Integer(4));// duplicate, is added 13
        System.out.println(list); 
     } 
}
