/*
Claudia Lorena Cervera Canchola
Ejercicio 10
Meta: Presentar la visualizacion de los componentes del paquete java.awt.*
Datos: Contenedores Frame y Buttons
Calculos: No hay puesto que no requiere eventos.
Salidas:Despliegue de los contenedores en la pantalla
Navegabilidad: Solo acciones de iconizacion
 */
package Ejercicio10;
import java.awt.*;

/**
 *
 * @author LORENAC2
 */
public class BorderExample {
    private Frame f;
    private Button bn, bs, bw, be, bc;
    
    public BorderExample() {
        f = new Frame("Border Layout");
        bn = new Button("B1"); 
        bs = new Button("B2");
        bw = new Button("B3"); 
        be = new Button("B4"); 
        bc = new Button("B5");
    }
    public void launchFrame() {
        f.add(bn, BorderLayout.NORTH); 
        f.add(bs, BorderLayout.SOUTH); 
        f.add(bw, BorderLayout.WEST); 
        f.add(be, BorderLayout.EAST);
        f.add(bc, BorderLayout.CENTER); 
        f.setSize(200,200); 
        f.setVisible(true);
    }
    public static void main(String args[]) {
        BorderExample guiWindow2 = new BorderExample();
        guiWindow2.launchFrame();
    }
}
