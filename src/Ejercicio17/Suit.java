/*
Claudia Lorena Cevera Canchola
Ejercicio 17
 */
package Ejercicio17;

/**
 *
 * @author LORENAC2
 */
public enum Suit { 
    SPADES   ("Spades"),
    HEARTS  ("Hearts"),
    CLUBS  ("Clubs"),
    DIAMONDS ("Diamonds");
    
private final String name;

    private Suit(String name) {
        this.name = name;
    } 
    
    public String getName() {
        return name;
    } 
}
