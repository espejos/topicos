/*
Claudia Lorena Cevera Canchola
Ejercicio 17
 */
package Ejercicio17;

/**
 *
 | @author LORENAC2
 */
public class TestPlayingCard {
    public static void main(String[] args) {
        PlayingCard card1= new PlayingCard(Suit.SPADES, 2);
        System.out.println("card1 is the " + card1.getRank() + " of " + card1.getSuit().getName());
        // PlayingCard card2 = new PlayingCard(47, 2);
        // This will not compile.
    }
}
