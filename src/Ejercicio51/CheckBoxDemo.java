/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio51;

import Ejercicio50.ButtonDemo;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author LeoNa
 */
public class CheckBoxDemo extends ButtonDemo{
    public JCheckBox jchkCentered = new JCheckBox("Centered");
    public JCheckBox jchkBold = new JCheckBox("Bold");
    public JCheckBox jchkItalic = new JCheckBox("Italic");
    
    public static void main(String[] args) {
        CheckBoxDemo frame = new CheckBoxDemo();
        frame.setTitle("CheckBoxDemo");
        frame.setSize(500, 200);
        frame.setLocationRelativeTo(null); // Center the frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
    
    public CheckBoxDemo(){
        jchkCentered.setMnemonic('C');
        jchkBold.setMnemonic('B');
        jchkItalic.setMnemonic('I');
        
        // Create a new panel to hold check boxes
        JPanel jpCheckBoxes = new JPanel();
        jpCheckBoxes.setLayout(new GridLayout(3, 1));
        jpCheckBoxes.add(jchkCentered);
        jpCheckBoxes.add(jchkBold);
        jpCheckBoxes.add(jchkItalic);
        add(jpCheckBoxes, BorderLayout.EAST);
        
        // Register listeners with the check boxes
        new ListenerCkeckBox(this);
    }
    
    public void setNewFont() {
        // Determine a font style
        int fontStyle = Font.PLAIN;
        fontStyle += (jchkBold.isSelected() ? Font.BOLD : Font.PLAIN);
        fontStyle += (jchkItalic.isSelected() ? Font.ITALIC : Font.PLAIN);
        // Set font for the message
        Font font = messagePanel.getFont();
        messagePanel.setFont(
                new Font(font.getName(), fontStyle, font.getSize()));
    }
}