/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio51;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author LeoNa
 */
public class ListenerCkeckBox {

    ListenerCkeckBox(CheckBoxDemo checkBox) {
        checkBox.jchkCentered.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkBox.messagePanel.setCentered(checkBox.jchkCentered.isSelected());
            }
        });
        
        checkBox.jchkBold.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkBox.setNewFont();
            }
        });
        
        checkBox.jchkItalic.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkBox.setNewFont();
            }
        });
    }
    
}
