/*
Claudia Lorena Cervera Canchola
Ejercicio 14
 */
package Ejercicio14;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 *
 * @author LORENAC2
 */
public class UnzipFiles {

    public static void main(String[] args) {
        UnzipFiles uf= new UnzipFiles();
        //String zipFilePath = "/Users/pankaj/tmp.zip";       
        //String destDir = "/Users/pankaj/output";     
        String zipFilePath ="E:\\Data.zip";
        String destDir="E:\\Data1";
        uf.unzip(zipFilePath, destDir);
    }

    private  void unzip(String zipFilePath, String destDir) {
        File dir = new File(destDir);
        //buffer for read and write data to file
        byte[] buffer = new byte[1024];
        // create output directory if it doesn't exist
        if(!dir.exists()) dir.mkdirs();         
        try {
           FileInputStream fis = new FileInputStream(zipFilePath);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry ze = zis.getNextEntry();

            while(ze != null){
                String fileName = ze.getName();
                File newFile = new File(destDir + File.separator + fileName);
                FileOutputStream fos= new FileOutputStream(newFile);
                int len;

                System.out.println("Unzipping to "+newFile.getAbsolutePath());
                //create directories for sub directories in zip
                new File(newFile.getParent()).mkdirs();
                while ((len = zis.read(buffer)) > 0) 
                	fos.write(buffer, 0, len);
                fos.close();
                //close this ZipEntry
                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            //close last ZipEntry
            zis.closeEntry();
            zis.close();
            fis.close();
        } catch (IOException e) { e.printStackTrace(); } 
    }
}