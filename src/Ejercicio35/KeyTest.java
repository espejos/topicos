/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio35;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/**
 *
 * @author LORENAC2
 */
public class KeyTest {
  public static void main(String args[]) {
    JFrame jfr = new JFrame("Key Text Sample");
    KeyTextComponent keyTtx = new KeyTextComponent();
    final JTextField jtf = new JTextField();
    Container cp = jfr.getContentPane();

    ActionListener actListener = new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        String keyText = actionEvent.getActionCommand();

        jtf.setText(keyText);
      }
    };

    keyTtx.addActionListener(actListener);
    cp.add(keyTtx, BorderLayout.CENTER);
    cp.add(jtf, BorderLayout.SOUTH);
    jfr.setSize(200, 200);
    jfr.setVisible(true);
  }
}

class KeyTextComponent extends Canvas {
    private ActionListener listnerList = null;

    public KeyTextComponent() {
      setBackground(Color.BLACK);
      KeyListener keyListener = new KeyAdapter() {
        @Override
        public void keyPressed(KeyEvent keyEvent) {
          if (listnerList != null) {
            int key = keyEvent.getKeyCode();
            String keystr = KeyEvent.getKeyText(key);
            ActionEvent aEvent = new ActionEvent(this, 
                                     ActionEvent.ACTION_PERFORMED, keystr);
            listnerList.actionPerformed(aEvent);
          }
        }
      };
      
      MouseListener mListner = new MouseAdapter() {
        @Override   
        public void mousePressed(MouseEvent mEvent) {  requestFocus();  }
      };

      addKeyListener(keyListener);
      addMouseListener(mListner);
    }

    public void addActionListener(ActionListener actionListn) {
      listnerList = AWTEventMulticaster.add(listnerList, actionListn);
    }

    public void removeActionListener(ActionListener actionListn) {
      listnerList = AWTEventMulticaster.remove(listnerList, actionListn);
    }

    @Override
    public boolean isFocusTraversable() {  return true;  }
}





