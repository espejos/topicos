/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio30;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
 /**
 *
 * @author LORENAC2
 */
public class ComponentListenerExample {
  public static void main(String [] args) {
 
    JFrame.setDefaultLookAndFeelDecorated(true);
    JFrame frame = new JFrame("ComponentListener Example");
    frame.setSize(300,150);
 
    final JLabel label = new JLabel();
    frame.add(label);
 
    frame.addComponentListener(new ComponentListener() {   //clase anonima
        public void componentHidden(ComponentEvent ce) {
          System.out.println("Component Hidden");
          label.setText("Component Hidden");
        }
        public void componentShown(ComponentEvent ce) {
          System.out.println("Component Shown");
          label.setText("Component Shown");
        }
        public void componentMoved(ComponentEvent ce) {
          label.setText("Component Moved");
        }
        public void componentResized(ComponentEvent ce) {
          label.setText("Component Resized");
        }
    });
    frame.setVisible(true);			
  }
}
