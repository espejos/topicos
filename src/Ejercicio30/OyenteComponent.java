/*
Claudia Lorena Cervera Canchola
Ejercicio 27
 */
package Ejercicio30;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JPanel;
/**
 *
 * @author LORENAC2
 */
public class OyenteComponent implements ComponentListener {
  ComponentEventDemo f;

  public OyenteComponent(ComponentEventDemo f) {  this.f = f;  }
  public void componentHidden(ComponentEvent e) {
    f.displayMessage("componentHidden event from "
                       + e.getComponent().getClass().getName());
  }

  public void componentMoved(ComponentEvent e) {
    Component c = e.getComponent();
  
    f.displayMessage("componentMoved event from "
                       + c.getClass().getName() 
                       + "; new location: "
                       + c.getLocation().x
                       + ", "
                       + c.getLocation().y);
  }

  public void componentResized(ComponentEvent e) {
    Component c = e.getComponent();

    f.displayMessage("componentResized event from "
                       + c.getClass().getName()
                       + "; new size: "
                       + c.getSize().width
                       + ", "
                       + c.getSize().height);
  }

  public void componentShown(ComponentEvent e) {
    f.displayMessage("componentShown event from "
                       + e.getComponent().getClass().getName());
  }
}

