/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio50;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

/**
 *
 * @author LeoNa
 */
public class ListenerButton implements ActionListener{
    
    ListenerButton(ButtonDemo buttonDemo){
        // Register listeners with the buttons
        buttonDemo.jbtLeft.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                buttonDemo.messagePanel.moveLeft();
            }
        });
        buttonDemo.jbtRight.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                buttonDemo.messagePanel.moveRight();
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
