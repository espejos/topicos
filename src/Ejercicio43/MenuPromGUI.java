/* MenuPromGUI.java 
   clase que controla la aplicacion del promedio grafico con persistencia
   Para ilustrar una aplicación con menus graficos 
   utiliza swing, 
   el 10 de marzo del 2011
   por cirino Silva Tovar
   ojo capa del usuario que implementa la navegavilidad, el control de eventos
   y la vista de la aplicacion. en pocas palabras la capa del usuario.
    Ejercicio 27
 */
package Ejercicio43;
import java.awt.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
/**
 *
 * @author LORENAC2
 */
class MenuPromGUI extends Frame implements ActionListener {  // vista y oyente
  private JOptionPane messageBox;                            // de eventos
  PromCentPersGUI pcp = new PromCentPersGUI();  // modelo del negocio
  ArchiDatosGUI ad = new ArchiDatosGUI();       // persistencia
  int n;

  public MenuPromGUI()    {  // constructor por defecto
    createMenu();
    setTitle("Promedio de n datos");
    addWindowListener(new CloseHandler());  // registra el oyente en el frame
    setLocation(200,200);                   // para el cierre de la ventana
    setSize(300,200);
  }

  public void actionPerformed(ActionEvent event)    {  //event handler
    String menuName;                                  // action

    menuName = event.getActionCommand();       // tipo de accion
    if (menuName.equals("Nuevo"))           newProm();
    else if (menuName.equals("Abrir..."))   open();        
    else if (menuName.equals("Guardar"))    save();        
    else if (menuName.equals("Salir"))      quit();        
    else if (menuName.equals("Captura"))    edit();        
    else if (menuName.equals("Resultados")) resultado();        
    else if (menuName.equals("Ayuda"))      ayuda();        
  }

  private void createMenu() {
    MenuItem    item;
    Menu        menu;
    MenuBar     menuBar;
    String [] bar  = {"Archivo","Entradas","Mostrar"};
    String [][]opt = {{"Nuevo","Abrir...","Guardar","Salir"},
                      {"Captura"},
                      {"Resultados","Ayuda"}};

    menuBar = new MenuBar(); //create menubar first
    for (int i = 0 ; i < bar.length; i++) {
      menu = new Menu(bar[i]);        //Show Menu
      menuBar.add(menu);               // agrega menu a la barra
      for (int j = 0; j < opt[i].length; j++ ) {
        item = new MenuItem(opt[i][j]);        //All Students
        item.addActionListener( this );     // registra oyente de accion
        menu.add( item );                   // agrega elemento al menu
      }
    }        
    setMenuBar(menuBar); //set menubar AFTER all menus are added to it
  }

  private void resultado( )    {
    pcp.calculos(n);
    pcp.resultados(n);
  }

  private void ayuda(){
    String l= "";
    l= l +"Cuando se solicite el nombre ";
    l= l +"de un archivo, usted puede dar <enter> ";
    l= l +"para visualizar el contenido ";
    l= l +"del subdirectorio de trabajo actual ";   
    messageBox.showMessageDialog(null,l);
  }

  private void edit(){   n = pcp.datos();   }

  private void newProm(){
    String l= "";

    n = pcp.nuevo();
    l = l+ "Se ha limpiado el arreglo";
    l = l+ "con cero datos, ahora hay disponibles";
    l = l+ "100 lugares para alimentar y promediar";
    messageBox.showMessageDialog(null,l);
  }

  private void open()    {
    String arch="";

    arch = messageBox.showInputDialog("Deme nombre del archivo:");
    if (arch.equals("")||arch == null) 
      ad.listaFicheros(".");
    else n = ad.datosIn(arch, pcp.datos);    
  }

  private void quit() {   System.exit(0);   }

  private void save() {
    String arch="";

    arch = messageBox.showInputDialog("Deme nombre del archivo:");
    if (arch.equals("")||arch == null) 
      ad.listaFicheros(".");
    else ad.datosOut(arch, pcp.datos, n);
  }

  public static void main (String arg[]){
    MenuPromGUI mpw = new MenuPromGUI();

    mpw.pcp.inicio();      // la navegabilidad se hace en el hilo de 
    mpw.setVisible(true);  // despacho de eventos
  }
  
  private class CloseHandler extends WindowAdapter {
    public void windowClosing(WindowEvent e) {  System.exit(0);  }
  }
}






