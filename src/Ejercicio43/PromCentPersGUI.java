/*PromCentPers.java
  Instituto Tecnológico de León
  Ingeniería en Sistemas Computacionales
  Programación orientada a objetos.
  Alumno (a):
  Foto Digital del alumno (a)
  Tarea #: _____-__
  Fecha de entrega: Febrero 5 del 2010
  Modificado 22 de febrero 2011
  Terminado el 25 de febrero 2011 muestra la interaccion entre objetos
  Modificado el 10 de Marzo del 2011 para que se comporte graficamente
    Ejercicio 27
 */
package Ejercicio43;

import javax.swing.JOptionPane;
import java.io.*;
/**
 *
 * @author LORENAC2
 */
class PromCentPersGUI {
  public static final String CENTINELA = "fin";
  double datos[] = new double[100];
  double prom, suma;
  JOptionPane box;
  
  void inicio() {   // despliega dialogo al inicio de la aplicacion
    String l="";

    l= l +"\nPara calcular el promedio de n datos";
    l= l +"Manipulando un arreglo y usando centinela";
    l= l +"para terminar la captura, se muestran los";
    l= l +"datos capturados\n";
    box.showMessageDialog(null,l);
  }

  int nuevo () {   // clarea el arreglo de datos
    datos = new double[100];
    return 0;
  }

  String capDato(int cont) {
    String d,aux="Deme el dato:";

    do d = box.showInputDialog(aux+cont+" o \"fin\" para terminar:");  
    while (!ArchiDatosGUI.isNum(d) && !d.equals(CENTINELA)); 
    return d;
  }

  int datos() {
    int cont = 0;
    String dato = capDato(cont);

    while(!dato.equals(CENTINELA) && cont <100) {
      datos[cont++] = Double.parseDouble(dato);
      dato = capDato(cont);
    }
    return cont;
  }

  void calculos(int cont) {
    String l= "No es posible calcular el promedio de 0 datos";

    suma = prom = 0.0;
    for (int i=0; i < cont; i++) 
      suma=suma+datos[i];
    if(cont!=0) 
      prom=suma/(cont);
    else 
      box.showMessageDialog(null,l);
  }

  void resultados(int cont) {
    String l;

    l="\nFueron "+ cont +" Datos capturados: { ";
    for (int i=0; i<cont; i++) 
      l=l+datos[i]+", ";
    l= l +"} \n\nLa suma es:"+suma;
    l= l +"\nY el promedio es:"+prom;
    box.showMessageDialog(null,l);
  }
}













