/*
Claudia Lorena Cervera Canchola
 Ejercicio 9
Meta: Presentar la visualizacion de los componentes del paquete java.awt.*
Datos: Contenedores Frame y Button
Calculos: No hay puesto que no requiere eventos.
Salidas:Despliegue de los contenedores en la pantalla
Navegabilidad: Solo acciones de iconizacion
 */
package Ejercicio9;

import java.awt.*;

/**
 *
 * @author LORENAC2
 */
public class LayoutExample {
   private Frame f;
   private Button b1; 
   private Button b2;

   public LayoutExample() { 
       f = new Frame("GUI example");
       b1 = new Button("Press Me"); 
       b2 = new Button("Don’t press Me");
   }
   public void launchFrame() { 
       f.setLayout(new FlowLayout());
       f.add(b1); 
       f.add(b2); 
       f.pack(); 
       f.setVisible(true);
   } 
   
   public static void main(String args[]) { 
       LayoutExample guiWindow = new LayoutExample();
       guiWindow.launchFrame(); 
   }
} 
