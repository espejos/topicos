/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejercicio52;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author LeoNa
 */
public class ListenerRadioButton {

    public ListenerRadioButton(RadioButtonDemo radioButton) {
        // Register listeners for radio buttons
        radioButton.jrbRed.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent e) {
                radioButton.messagePanel.setForeground(Color.RED);
            }
        });
        
        radioButton.jrbGreen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                radioButton.messagePanel.setForeground(Color.green);
            }
        });
        
        radioButton.jrbBlue.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                radioButton.messagePanel.setForeground(Color.blue);
            }
        });
    }
    
}
