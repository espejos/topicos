package Ejercicio52;

import Ejercicio51.CheckBoxDemo;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

/**
 *
 * @author LeoNa
 */
public class RadioButtonDemo extends CheckBoxDemo{
    public JRadioButton jrbRed, jrbGreen, jrbBlue;

// Declare radio buttons
    public static void main(String[] args) {
        RadioButtonDemo frame = new RadioButtonDemo();
        frame.setSize(500, 200);
        frame.setLocationRelativeTo(null); // Center the frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("RadioButtonDemo");
        frame.setVisible(true);
    }
    
    public RadioButtonDemo() {
        // Create a new panel to hold check boxes
        JPanel jpRadioButtons = new JPanel();
        jpRadioButtons.setLayout(new GridLayout(3, 1));    
        jpRadioButtons.add(jrbRed = new JRadioButton("Red"));
        jpRadioButtons.add(jrbGreen = new JRadioButton("Green"));
        jpRadioButtons.add(jrbBlue = new JRadioButton("Blue"));
        
        // Create a radio-button group to group three buttons
        add(jpRadioButtons, BorderLayout.WEST);
        ButtonGroup group = new ButtonGroup();
        group.add(jrbRed);
        group.add(jrbGreen);
        group.add(jrbBlue);
        
        // Set keyboard mnemonics
        jrbRed.setMnemonic('E');
        jrbGreen.setMnemonic('G');
        jrbBlue.setMnemonic('U');
        
        // Set initial message color to blue
        jrbBlue.setSelected(true);
        messagePanel.setForeground(Color.blue);
        
        new ListenerRadioButton(this);

    }
}