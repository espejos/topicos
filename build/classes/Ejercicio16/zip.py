# Ejemplo obtenido de la página
# https://www.pythonmania.net/es/2017/06/25/archivos-zip-en-python/

import zipfile
try:
    import zlib
    compression = zipfile.ZIP_DEFLATED
except:
    compression = zipfile.ZIP_STORED

zf = zipfile.ZipFile("example.zip", mode="w")

try:
    zf.write("test.txt", compress_type=compression)
finally:
    zf.close()
