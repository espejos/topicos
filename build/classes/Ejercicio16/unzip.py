# Ejemplo obtenido de la página
# https://appdividend.com/2020/01/31/python-unzip-how-to-extract-single-or-multiple-files/

import zipfile
import os
# Impresión de la lista de elementos en el directorio de trabajo actual antes
# de extraer el contenido del archivo zip
print("Lista de elementos en el directorio antes de la extracción")
for item in os.listdir(path='.'):
    print(item)
print("\n\n")
# Abrir el archivo zip en modo de lectura.
# Extraer todo el contenido del archivo zip en el directorio de trabajo actual.
with zipfile.ZipFile("example.zip","r") as zf:
    zf.extractall()
# Impresión de la lista de elementos en el directorio de trabajo actual antes de extraer contenido
print("Lista de elementos en el directorio después de la extracción")
for item in os.listdir(path='.'):
    print(item)